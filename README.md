### Rhythm Runner ###

### Summary ###

Rhythm Runner was a final games project for ITP-380, Introduction to Game Programming. It was a collaboration between four students, written in C# and using Visual Studio 2010 and XNA. 
Rhythm Runner is a rhythm game where the player navigates a ship to move on the beat. It featured two different game modes, using the number pad and arrow keys. It also had a map editor for players to add their own songs and create tracks to include in the game.


###To Run ###
To run the project, please open the solution file in Visual Studio 2010 with Microsoft XNA and run through Visual Studio. 